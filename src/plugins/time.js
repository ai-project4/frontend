export default {
  install: (app) => {
    const methods = {
      wait: (ms) =>
        new Promise((resolve) => {
          setTimeout(resolve, ms);
        }),
    };

    app.provide("time", methods);
  },
};
