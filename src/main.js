import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import axios from "./plugins/axios";
import time from "./plugins/time";

import "./assets/styles/main.scss";

createApp(App).use(store).use(router).use(axios).use(time).mount("#app");
